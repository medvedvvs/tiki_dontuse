{* $Id$ *}
<div class="row justify-content-center">
	<div class="col-sm-6">
		{module module=login_box
			mode="module"
			show_register="y"
			show_forgot="y"
			error=""
			flip=""
			decorations=""
			nobox=""
			notitle=""
		}
	</div>
</div>
